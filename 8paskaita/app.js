// darbas su jquery - $
// $() === document.querySelector()


// $(document).ready(function() {
// 	console.log('jquery veikia');
// });

/*
	jquery alternatyva javascript selektoriams ir stiliui
 */

// js parasymas
// document.getElementById('antraste').style.color = 'white';
// document.getElementById('antraste').style.fontSize = '50px';
// document.getElementById('antraste').style.backgroundColor = 'black';

// $('#antraste').css({
// 	fontSize: '50px',
// 	color: 'white',
// 	backgroundColor: 'black'
// });


// js klasiu pridejimas
// document.getElementById('antraste').classList.add('klases-pavadinimas');

// DRY - Dont repeat yourself
// var a = $('#antraste');

// a.addClass('klases-pavadinimas');
// a.removeClass('klases-pavadinimas');
// a.toggleClass('klases-pavadinimas');



// eventam
// document.getElementById('antraste').addEventListener('click', function() {});

// $('#antraste').on('keypress', function() {
// 	console.log('click');
// 	// $(this).hide();
	
// 	// $('ul').append('<li>Elementas</li>');
// 	$('ul').prepend('<li>Elementas</li>');


// });

// $('input[type=text]').on('click', function() {
// 	console.log('click');
// 	$(this).hide();
// });


// var masyvas = [1, 2, 3, 4, 5];

// $.each(masyvas, function(index, el) {
// 	console.log('Index - ' + index + '. Reiksme - ' + el);
// });


// $(document).ready(function(){
//   $(".owl-carousel").owlCarousel({
//   	loop: true, 
//   	autoplay: true,
//   	responsive:{
//         0:{
//             items:1,
//             nav:true
//         },
//         900:{
//             items:2,
//             nav:false
//         },
//         1200:{
//             items:5,
//             nav:true,
//             loop:true
//         }
//     }
//   });
// });


// $('#click').on('click', function() {
// 	$("html, body").animate({ scrollTop: 0 }, 600);
// });


// 1 uzduotis
function alaus() {
	var message = 'einam alaus. Verygai nesakykit';
	console.log(message);

	document.querySelector('h1').innerText = message;
}

// 2 uzduotis
var btn = document.getElementById('btn');

btn.addEventListener('click', function() {
	alaus();
});