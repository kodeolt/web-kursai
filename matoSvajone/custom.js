// Paimame canvas elementa pagal zyme
var canvas = document.querySelector('canvas');
var c = canvas.getContext('2d');

// Lango dydis ir canvas dydi nustatom
var maxX = canvas.width = window.innerWidth;
var maxY = canvas.height = window.innerHeight;

var min = 100;

var mouse = {
	x: undefined,
	y: undefined
}


// Objekto kurimo funkcija
function Circle(x, y, dx, dy, radius, color) {
	this.x = x;
	this.y = y;
	this.dx = dx;
	this.dy = dy;
	this.radius = radius;
	this.minRadius = radius;
	this.color = color;

	// Piesimo funkcija
	this.draw = function() {
		c.beginPath();
		c.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
		c.fillStyle = this.color;
		c.fill();
	} 

	// Atnaujinimo funkcija
	// kuri judins kamuoliuka ir tikrins rezius
	this.update = function() {
		// Koordinaciu skaiciavimas
		
		if(this.x + this.radius > maxX || this.x - this.radius < 0) {
			this.dx = -this.dx;
		}

		if(this.y + this.radius > maxY || this.y - this.radius < 0) {
			this.dy = -this.dy;
		}

		this.x += this.dx;
	      this.y += this.dy;

		if(mouse.x - this.x < 50 && mouse.x - this.x > -50
	      && mouse.y - this.y < 50 && mouse.y - this.y > -50) {
	      if(this.radius < 100) {
	        this.radius += 3;
	      }
	      this.x += this.dx;
	      this.y += this.dy;
	    } else if(this.radius > this.minRadius && this.radius > 1) {
	      this.radius -= 1;
	    }

		// Perpiesimas
		this.draw();
	}
	
}


// visi kamuoliukai
var circleArray = [];

// Paspaudus ant canvas elemento
// pridedam nauja kamuoliuka
canvas.addEventListener('click', function() {

	// random savybes kamuoliuko
	
	// random x ir y koordinates
	// var ran_x = Math.random() * 100 + min;
	// var ran_y = Math.random() * 100 + min;
	

	var ran_dx = Math.random() * 10 - 5;
	var ran_dy = Math.random() * 10 - 5;
	var ran_radius = Math.random() * 50;
	var ran_color = '#' + Math.random().toString(16).substr(-6);

	// new Circle()
	circleArray.push(new Circle(mouse.x, mouse.y, ran_dx, ran_dy, ran_radius, ran_color));

});

function animate() {
	// Isvalom kadrus
	c.clearRect(0, 0, maxX, maxY);

	// Piesiam elementus
	for(var i = 0; i < circleArray.length; i++) {
		circleArray[i].update();
	}

	// infinity loop funkcijos animate
	requestAnimationFrame(animate);
}

// Pirmas iskvietimas
animate();

window.addEventListener('mousemove', function(e) {
	// console.log(e);

	mouse.x = e.clientX;
	mouse.y = e.clientY;
});



