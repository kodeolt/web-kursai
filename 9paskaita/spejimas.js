// 1. sugeneruoti random skaiciu.
// 			1-6
// 2. Paspaudus mygtuka mum reikes
// 			iskviesti langa, kuriame
// 			spesime.
// 3. Spausdinimas
// 4. For fun. Gyvybes 


// random() : 0-1 : 0.0000000125
var randomNumber = Math.floor(Math.random() * 6 + 1);

// Gyvybes ir ju spausdinimas svetaineje
var lives = 3;
var livesContainer = document.getElementById('spejimai');
livesContainer.innerText = lives;


var messageContainer = document.getElementById('zinute');

var startGame = document.getElementById('spek');
startGame.addEventListener('click', function() {
	// iskviesti musu langa
	// vartotojas ives skaiciu
	// konvertuosim i integer
	// lyginsim su random skaicium

	var guess = parseInt(prompt('Iveskite skaiciu (1-6):'));
	var message;
	// Ar ivestas skaicius yra tarp 1-6
	if(guess > 0 && guess <= 6) {

		// Ar vartotojo skaicius yra lygus musu random skaiciui
		if(guess === randomNumber && lives >= 1) {
			message = 'atspejote skaiciu';
		} else if(guess > randomNumber && lives >= 1) {
			message = 'Jusu spejimas yra didesnis. bandykite dar karta';
			lives--;
		} else if(guess < randomNumber && lives >= 1) {
			message = 'Jusu spejimas yra mazesnis. bandykite dar karta';
			lives--;
		} else {
			message = 'Gyvybes baigesi';
		}

		livesContainer.innerText = lives;
		messageContainer.innerText = message;

	} else {
		guess = parseInt(prompt('Blogai ivestas skaicius'));
	}


});
