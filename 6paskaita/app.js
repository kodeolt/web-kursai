// 'use strict';

// Kintamuju kurimas
// console.log('JS faile veikia');

// var x = '25';
// x = parseInt(x);

// var tekstas = 'Labas kebabas';

// var p = x *20;

// console.log(p);



// Salyginiai sakiniai, ciklai, inkrementai, dekrementai

// Salyginis sakinys
// if(true) {
// 	// jei tiesa
// } else if(papildomaSalyga) {
// 	// Jei tiesa
// } else {
// 	// jei netenkina virsuj esanciu salygu
// }


// switch case
// switch(x) {
// 	case 1:
// 		//Kodas
// 		break;
// 	case 2: 

// 	break;

// 	default:
// 		// pagal nutylejima
// }

// // for ciklas
// for(var i = 0; i <= 10; i++) {
// 	console.log('spausdinam ' + i);
// }

// // while ciklas

// while(x < 10) {

// }


// Math
// DOM
// ternary operators

// var text = 'kazkas';

// text.toUpperCase();
// var kint = false;

// kint ? console.log('Tiesa') : console.log('Netiesa');

// skaiciu nuo 0 - 1
// console.log(Math.floor(Math.random() * 10 + 1));


// DOM
// Document Object Model


// Selektorius pagal ID
// var elemId = document.getElementById('heading');
// elemId.innerText = '<?>Naujas tekstas';
// elemId.innerHtml = '<em>Kitas tekstas</em>';
// console.log(elemId.innerText);


// css: background-color
// js: backgroundColor
// elemId.style.backgroundColor = "red"; 
// elemId.style.fontSize = "50px"; 

// console.log('pirmas');
// // klases pridejimas
// elemId.classList.add('tekstas');

// elemId.className += ' tekstas';


// Selektorius pagal klases
// var elemClass = document.getElementsByClassName('item');

// for(var i = 0; i < elemClass.length; i++)
// 	elemClass[i].style.color = "white";

// console.log(elemClass);


// Selektorius pagal zymes
// var elemTag = document.getElementsByTagName('span');
// elemTag[0].style.fontSize = "100px";

// console.log(elemTag);

// Query Selector
// var elemCss = document.querySelector('.content h3');

// // elemCss.style.backgroundColor = "yellow";
// elemCss.style.fontSize = "50px";

// var x = parseInt(prompt('Iveskite skaiciu nuo 1 - 3:'));
// var elem = document.getElementsByClassName('item');

// // == vs ===
// // == tik reiksmes svarbios
// // === reiksme ir tipas svarbu
// if(x === 1) {
// 	elem[0].style.fontSize = "100px";
// } else if(x === 2) {
// 	elem[1].style.fontSize = "70px";
// } else if(x === 3) {
// 	elem[2].style.fontSize = "150px";
// }

// Nervint zmonem
// while(x != "slaptazodis") {
// 	x = prompt('Ivesk slaptazodi');
// }




// Objektai

// objekto sukurimas
// var pc = {};
// pc.os = 'Windows';
// pc.ram = 16;

// pc.gpu = ['Intel', 'Nvidia'];

// console.log(pc.gpu[1]);



// if(pc.ram > 8) {
// 	print(pc.ram);
// }

var elem = document.getElementById('heading');

var i = 0;

elem.addEventListener('click', function() {
	// Kodas vykdomas, kai paspaudziama ant headingo
	console.log(++i);
	// elem.style.backgroundColor = 'red';
	
	elem.classList.toggle('tekstas');

});

 


// function print() {
	
// }